package com.devcamp.bookchaptercrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookChapterCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookChapterCrudApplication.class, args);
	}

}
