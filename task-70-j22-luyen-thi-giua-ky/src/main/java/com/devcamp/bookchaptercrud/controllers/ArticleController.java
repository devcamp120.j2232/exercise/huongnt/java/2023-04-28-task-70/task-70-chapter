package com.devcamp.bookchaptercrud.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.bookchaptercrud.model.CArticle;
import com.devcamp.bookchaptercrud.model.CChapter;
import com.devcamp.bookchaptercrud.repository.IArticleRepository;
import com.devcamp.bookchaptercrud.repository.IChapterRepository;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    IArticleRepository articleRepository;

    @GetMapping("/all")
    public ResponseEntity<List<CArticle>> getAllArticle(){
        try {
            List<CArticle> articleList = new ArrayList<>();
            articleRepository.findAll().forEach(articleList::add);
            return new ResponseEntity<>(articleList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("detail/{id}")
    public ResponseEntity<CArticle> getArticleById(@PathVariable long id){
        try {
            CArticle article = articleRepository.findById(id);
            if (article != null){
                return new ResponseEntity<>(article, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    IChapterRepository chapterRepository;
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createArticle(@PathVariable("id") long id, @RequestBody CArticle pArticle){
        try {
            CChapter chapter = chapterRepository.findById(id);
            if (chapter != null){
                CArticle newArticle = new CArticle();
                newArticle.setCode(pArticle.getCode());
                newArticle.setName(pArticle.getName());
                newArticle.setIntroduction(pArticle.getIntroduction());
                newArticle.setPage(pArticle.getPage());
                newArticle.setChapter(chapter);
                CArticle _article = articleRepository.save(newArticle);
                return new ResponseEntity<>(_article, HttpStatus.CREATED);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Article: "+e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<CArticle> updateArticleById(@PathVariable("id") long id, @RequestBody CArticle pArticle){
        try {
            CArticle article = articleRepository.findById(id);
            if (article != null){
                article.setCode(pArticle.getCode());
                article.setName(pArticle.getName());
                article.setIntroduction(pArticle.getIntroduction());
                article.setPage(pArticle.getPage());
                
                return new ResponseEntity<>(articleRepository.save(article), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CArticle> deleteArticleById(@PathVariable("id") long id) {
		try {
			articleRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}
